// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;
	FoodMesh = CreateDefaultSubobject<UStaticMeshComponent>("FoodMesh");
	FoodMesh->SetupAttachment(Root);
	//FoodMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	RotationRate = 100;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//GetWorld()->GetTimerManager().SetTimer(FoodTimerHandle, this, &AFood::DeSpawnFood, 4.0f, false);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalRotation(FRotator(0, RotationRate * DeltaTime, 0));

}

void AFood::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
			Snake->SpawnFood();
			if ((FMath::RandRange(0, 100)) > 90)
			{
				Snake->SpawnBonus();
			}
			if ((FMath::RandRange(0, 100)) > 90)
			{
				Snake->SpawnBonusWall();
			}
		}
	}
}

//void AFood::DeSpawnFood()
//{
//	&ASnakeBase::SpawnFood;
//	Destroy();
//}

