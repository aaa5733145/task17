// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Math/UnrealMathUtility.h"
#include "Food.h"
#include "BonusSpeed.h"
#include "BonusWallBase.h"
#include "WallBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.2;
	LastMoveDirection = EMovmentDirection::DOWN;
	Health = 3;
	Score = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	SpawnFood();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		if (ElementsNum > 1)
		{ 
			FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
			if (ElementIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
		else
		{
			auto PrevElement = SnakeElements[1];
			FVector PrevLocation = PrevElement->GetActorLocation();
			FTransform NewTransform(PrevLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
			Score++;
			Health = 1;
		}
	}
}

void ASnakeBase::Move()
{

	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovmentDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovmentDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovmentDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovmentDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToogleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i) 
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToogleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::AddBonusSpeed(float BonusSpeed)
{
	GetWorld()->GetTimerManager().SetTimer(BonusTimerHandle,this,&ASnakeBase::SetDefaultSpeed,10.0f,false);
	SetActorTickInterval(BonusSpeed);
}

void ASnakeBase::DestroyAllElements()
{
	IsLose = true;
	for (int i = SnakeElements.Num()-1; i >= 0; --i)
	{
		SnakeElements[i]->Destroy();
	}

}

void ASnakeBase::SpawnFood()
{
	float NewXLocation = FMath::RandRange(-9, 9) * 60;
	float NewYLocation = FMath::RandRange(-9, 9) * 60;
	FVector NewLocation(NewXLocation, NewYLocation, 0);
	if (SnakeElements[0]->GetActorLocation() != NewLocation) 
	{
		FTransform NewTransform(NewLocation);
		GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	}
	else
	{
		SpawnFood();
	}
}

void ASnakeBase::SpawnBonus()
{
	float NewXLocation = FMath::RandRange(-9, 9) * 60;
	float NewYLocation = FMath::RandRange(-9, 9) * 60;
	FVector NewLocation(NewXLocation, NewYLocation, 0);
	if (SnakeElements[0]->GetActorLocation() != NewLocation) 
	{
		FTransform NewTransform(NewLocation);
		GetWorld()->SpawnActor<ABonusSpeed>(BonusClass, NewTransform);
	}
	else
	{
		SpawnBonus();
	}
}

void ASnakeBase::SetDefaultSpeed()
{
	SetActorTickInterval(0.2);
}

void ASnakeBase::SpawnBonusWall()
{
	if(Score < 38)
	{
		float NewXLocation = FMath::RandRange(-9, 9) * 60;
		float NewYLocation = FMath::RandRange(-9, 9) * 60;
		FVector NewLocation(NewXLocation, NewYLocation, 0);
		if (SnakeElements[0]->GetActorLocation() != NewLocation)
		{
			FTransform NewTransform(NewLocation);
			GetWorld()->SpawnActor<ABonusWallBase>(BonusWallClass, NewTransform);
		}
		else
		{
			SpawnBonusWall();
		}
	}
}

void ASnakeBase::AddBonusWall(int WallsNum)
{
	for (int i = 0; i < WallsNum; ++i)
	{
		float NewXLocation = FMath::RandRange(-9, 9) * 60;
		float NewYLocation = FMath::RandRange(-9, 9) * 60;
		FVector NewLocation(NewXLocation, NewYLocation, 0);
		if (SnakeElements[0]->GetActorLocation() != NewLocation)
		{
			FTransform NewTransform(NewLocation);
			GetWorld()->SpawnActor<AWallBase>(WallClass, NewTransform);
		}
		else
		{
			AddBonusWall();
		}
	}
}
