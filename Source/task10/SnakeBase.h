// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class ABonusSpeed;
class ABonusWallBase;
class AWallBase;
class AFood;


UENUM()
enum class EMovmentDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class TASK10_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonusSpeed> BonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonusWallBase> BonusWallClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallBase> WallClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Score;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovmentDirection LastMoveDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsLose = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	FTimerHandle BonusTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
	void AddBonusSpeed(float BonusSpeed = 0.15);
	UFUNCTION()
	void DestroyAllElements();
	UFUNCTION(BlueprintCallable)
	void SpawnFood();
	UFUNCTION(BlueprintCallable)
	void SpawnBonus();
	UFUNCTION()
	void SetDefaultSpeed();
	UFUNCTION(BlueprintCallable)
	void SpawnBonusWall();
	UFUNCTION(BlueprintCallable)
	void AddBonusWall(int WallsNum = 1);
};
