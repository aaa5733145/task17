// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusWallBase.h"
#include "SnakeBase.h"

// Sets default values
ABonusWallBase::ABonusWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;
	BonusWallMesh = CreateDefaultSubobject<UStaticMeshComponent>("BonusWallMesh");
	BonusWallMesh->SetupAttachment(Root);
	//BonusWallMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	RotationRate = 100;

}

// Called when the game starts or when spawned
void ABonusWallBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalRotation(FRotator(0, RotationRate * DeltaTime, 0));
}

void ABonusWallBase::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddBonusWall(3);
			Destroy();
		}
	}
}

